#& es-export-bulk --url http://localhost:9200 --file backup.json --index ontology_db --size 5000
Param (
    [string]$serverUrl,
    [string]$index,
    [string]$backupPath
)
$url = $serverUrl + $index
$backupFile = $index + ".json"
$destPath = (Join-Path $backupPath $backupFile)
$postFix = (Get-Date -format "yyyyMMdd_HHmmss")

Get-ChildItem $backupPath -Filter *.json |
ForEach-Object {
    if ($_.Name -eq $backupFile)
    {
        

        $backupBackupFile = $index + $postFix + ".json"
        $renameDst = (Join-Path $backupPath $backupBackupFile)

        Write-Host "Renaming $destPath -> $renameDst"

        Move-Item $destPath $renameDst 
    }
}


Write-Host "Backup $url to $destPath.json"

& elasticdump --input=$url --output=$destPath --type=data

#
#& elasticdump --input=http://localhost:9200/6f6418ad4f5c4a53baf4f4ae71b48b8174f38566eda64c76b862e839b6a8d07d --output=L:\Backup\outlook.json --type=data
#& elasticdump --input=http://localhost:9200/htmleditorcontroller --output=L:\Backup\htmleditorcontroller.json --type=data
