﻿
$tempDir = [Environment]::ExpandEnvironmentVariables("%IONTO_INST%")

if (Test-Path -Path $tempDir) 
{
	Write-Host "Create Zip-File"
    Get-ChildItem -Recurse $tempDir -File | ?{ $_.Name -eq "Version.txt" } |  % { 
        $version = %{Get-Content $_.FullName }
        $folder = $_.Directory.FullName
        $fileName = $_.FullName.Replace($tempDir,"")
        $fileName = $fileName.Replace("\Version.txt","")
        $splitFileNames = $fileName.Split("\")
        $fileName = $splitFileNames[$splitFileNames.Length-1]
        Write-Host $fileName " : " $version
        $cmd = 'c:\Program Files\7-Zip\7z'
        $zipFile = $tempDir + "\$fileName" + '_' + $version + '.exe'
        
	    $param = 'a', '-mmt' ,'-mx5', '-sfx7z.sfx', '-r', $zipFile, $folder
        #Write-Host $param
        & $cmd $param
	    Write-Host "success!"
    }
	
	# $zipFile = $tempDir + '_' + $version + '.exe'
	# $zipSourcePath = $dstDir + '\*.*'
    # $cmd = 'c:\Program Files\7-Zip\7z'
	# $param = 'a', '-mmt' ,'-mx5', '-sfx7z.sfx', '-r', $zipFile, $zipSourcePath
    # & $cmd $param
	# Write-Host "success!"
}

