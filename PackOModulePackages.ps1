﻿param (
    [string]$rootDir = "C:\Gitlab\OntologyWebControllers",
    [string]$destDir = "C:\Temp\NugetOModulePackages",
    [string]$nugetBinFolder = "C:\Nuget"    
)

$nugetExe = "$nugetBinFolder\Nuget.exe"
cd "C:\Temp\NugetOModulePackages"
& $nugetExe pack "$rootDir\ImportExport-Module\ImportExport-Module\ImportExport-Module.csproj"