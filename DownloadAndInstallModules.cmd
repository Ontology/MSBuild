c:\
cd \cygwin64\bin
mkdir %temp%\OModules
del /s /q %temp%\OModules\*.*
if /i [Appointment-Module] EQU [Ontolog-Module] (
	SET DOWNLOAD_URL=https://sourceforge.net/projects/ontologymanager/files/OntologyManager
) else (
	SET DOWNLOAD_URL=https://sourceforge.net/projects/ontologymanager/files/Modules/Appointment-Module
)

del /s /q "%OMODULE_PATH%\Appointment-Module\*.*"

wget -O "%temp%\OModules\Appointment-Module_0.3.4.213.exe" %DOWNLOAD_URL%/Appointment-Module_0.3.4.213.exe/download
"%PROGRAMFILES%\7-Zip\7z.exe" x "%temp%\OModules\Appointment-Module_0.3.4.213.exe" -o"%OMODULE_PATH%\Appointment-Module\"

if /i [AudioPlayer-Module] EQU [Ontolog-Module] (
	SET DOWNLOAD_URL=https://sourceforge.net/projects/ontologymanager/files/OntologyManager
) else (
	SET DOWNLOAD_URL=https://sourceforge.net/projects/ontologymanager/files/Modules/AudioPlayer-Module
)

del /s /q "%OMODULE_PATH%\AudioPlayer-Module\*.*"

wget -O "%temp%\OModules\AudioPlayer-Module_0.3.3.163.exe" %DOWNLOAD_URL%/AudioPlayer-Module_0.3.3.163.exe/download
"%PROGRAMFILES%\7-Zip\7z.exe" x "%temp%\OModules\AudioPlayer-Module_0.3.3.163.exe" -o"%OMODULE_PATH%\AudioPlayer-Module\"

if /i [BankTransaction-Module] EQU [Ontolog-Module] (
	SET DOWNLOAD_URL=https://sourceforge.net/projects/ontologymanager/files/OntologyManager
) else (
	SET DOWNLOAD_URL=https://sourceforge.net/projects/ontologymanager/files/Modules/BankTransaction-Module
)

del /s /q "%OMODULE_PATH%\BankTransaction-Module\*.*"

wget -O "%temp%\OModules\BankTransaction-Module_0.3.4.167.exe" %DOWNLOAD_URL%/BankTransaction-Module_0.3.4.167.exe/download
"%PROGRAMFILES%\7-Zip\7z.exe" x "%temp%\OModules\BankTransaction-Module_0.3.4.167.exe" -o"%OMODULE_PATH%\BankTransaction-Module\"

if /i [Bill-Module] EQU [Ontolog-Module] (
	SET DOWNLOAD_URL=https://sourceforge.net/projects/ontologymanager/files/OntologyManager
) else (
	SET DOWNLOAD_URL=https://sourceforge.net/projects/ontologymanager/files/Modules/Bill-Module
)

del /s /q "%OMODULE_PATH%\Bill-Module\*.*"

wget -O "%temp%\OModules\Bill-Module_0.3.3.226.exe" %DOWNLOAD_URL%/Bill-Module_0.3.3.226.exe/download
"%PROGRAMFILES%\7-Zip\7z.exe" x "%temp%\OModules\Bill-Module_0.3.3.226.exe" -o"%OMODULE_PATH%\Bill-Module\"

if /i [Change-Module] EQU [Ontolog-Module] (
	SET DOWNLOAD_URL=https://sourceforge.net/projects/ontologymanager/files/OntologyManager
) else (
	SET DOWNLOAD_URL=https://sourceforge.net/projects/ontologymanager/files/Modules/Change-Module
)

del /s /q "%OMODULE_PATH%\Change-Module\*.*"

wget -O "%temp%\OModules\Change-Module_0.3.3.235.exe" %DOWNLOAD_URL%/Change-Module_0.3.3.235.exe/download
"%PROGRAMFILES%\7-Zip\7z.exe" x "%temp%\OModules\Change-Module_0.3.3.235.exe" -o"%OMODULE_PATH%\Change-Module\"

if /i [Checklist-Module] EQU [Ontolog-Module] (
	SET DOWNLOAD_URL=https://sourceforge.net/projects/ontologymanager/files/OntologyManager
) else (
	SET DOWNLOAD_URL=https://sourceforge.net/projects/ontologymanager/files/Modules/Checklist-Module
)

del /s /q "%OMODULE_PATH%\Checklist-Module\*.*"

wget -O "%temp%\OModules\Checklist-Module_0.3.3.288.exe" %DOWNLOAD_URL%/Checklist-Module_0.3.3.288.exe/download
"%PROGRAMFILES%\7-Zip\7z.exe" x "%temp%\OModules\Checklist-Module_0.3.3.288.exe" -o"%OMODULE_PATH%\Checklist-Module\"

if /i [ClassTreeController] EQU [Ontolog-Module] (
	SET DOWNLOAD_URL=https://sourceforge.net/projects/ontologymanager/files/OntologyManager
) else (
	SET DOWNLOAD_URL=https://sourceforge.net/projects/ontologymanager/files/Modules/ClassTreeController
)

del /s /q "%OMODULE_PATH%\ClassTreeController\*.*"

wget -O "%temp%\OModules\ClassTreeController_0.1.0.0.exe" %DOWNLOAD_URL%/ClassTreeController_0.1.0.0.exe/download
"%PROGRAMFILES%\7-Zip\7z.exe" x "%temp%\OModules\ClassTreeController_0.1.0.0.exe" -o"%OMODULE_PATH%\ClassTreeController\"

if /i [ClipBoardListener-Url-Connector] EQU [Ontolog-Module] (
	SET DOWNLOAD_URL=https://sourceforge.net/projects/ontologymanager/files/OntologyManager
) else (
	SET DOWNLOAD_URL=https://sourceforge.net/projects/ontologymanager/files/Modules/ClipBoardListener-Url-Connector
)

del /s /q "%OMODULE_PATH%\ClipBoardListener-Url-Connector\*.*"

wget -O "%temp%\OModules\ClipBoardListener-Url-Connector_0.1.2.75.exe" %DOWNLOAD_URL%/ClipBoardListener-Url-Connector_0.1.2.75.exe/download
"%PROGRAMFILES%\7-Zip\7z.exe" x "%temp%\OModules\ClipBoardListener-Url-Connector_0.1.2.75.exe" -o"%OMODULE_PATH%\ClipBoardListener-Url-Connector\"

if /i [CommandLineCL-Module] EQU [Ontolog-Module] (
	SET DOWNLOAD_URL=https://sourceforge.net/projects/ontologymanager/files/OntologyManager
) else (
	SET DOWNLOAD_URL=https://sourceforge.net/projects/ontologymanager/files/Modules/CommandLineCL-Module
)

del /s /q "%OMODULE_PATH%\CommandLineCL-Module\*.*"

wget -O "%temp%\OModules\CommandLineCL-Module_0.3.3.146.exe" %DOWNLOAD_URL%/CommandLineCL-Module_0.3.3.146.exe/download
"%PROGRAMFILES%\7-Zip\7z.exe" x "%temp%\OModules\CommandLineCL-Module_0.3.3.146.exe" -o"%OMODULE_PATH%\CommandLineCL-Module\"

if /i [CommandLineRun-Module] EQU [Ontolog-Module] (
	SET DOWNLOAD_URL=https://sourceforge.net/projects/ontologymanager/files/OntologyManager
) else (
	SET DOWNLOAD_URL=https://sourceforge.net/projects/ontologymanager/files/Modules/CommandLineRun-Module
)

del /s /q "%OMODULE_PATH%\CommandLineRun-Module\*.*"

wget -O "%temp%\OModules\CommandLineRun-Module_0.3.3.153.exe" %DOWNLOAD_URL%/CommandLineRun-Module_0.3.3.153.exe/download
"%PROGRAMFILES%\7-Zip\7z.exe" x "%temp%\OModules\CommandLineRun-Module_0.3.3.153.exe" -o"%OMODULE_PATH%\CommandLineRun-Module\"

if /i [ControllerModelManager] EQU [Ontolog-Module] (
	SET DOWNLOAD_URL=https://sourceforge.net/projects/ontologymanager/files/OntologyManager
) else (
	SET DOWNLOAD_URL=https://sourceforge.net/projects/ontologymanager/files/Modules/ControllerModelManager
)

del /s /q "%OMODULE_PATH%\ControllerModelManager\*.*"

wget -O "%temp%\OModules\ControllerModelManager_0.0.0.3.exe" %DOWNLOAD_URL%/ControllerModelManager_0.0.0.3.exe/download
"%PROGRAMFILES%\7-Zip\7z.exe" x "%temp%\OModules\ControllerModelManager_0.0.0.3.exe" -o"%OMODULE_PATH%\ControllerModelManager\"

if /i [Database-Configurator-Module] EQU [Ontolog-Module] (
	SET DOWNLOAD_URL=https://sourceforge.net/projects/ontologymanager/files/OntologyManager
) else (
	SET DOWNLOAD_URL=https://sourceforge.net/projects/ontologymanager/files/Modules/Database-Configurator-Module
)

del /s /q "%OMODULE_PATH%\Database-Configurator-Module\*.*"

wget -O "%temp%\OModules\Database-Configurator-Module_0.3.3.136.exe" %DOWNLOAD_URL%/Database-Configurator-Module_0.3.3.136.exe/download
"%PROGRAMFILES%\7-Zip\7z.exe" x "%temp%\OModules\Database-Configurator-Module_0.3.3.136.exe" -o"%OMODULE_PATH%\Database-Configurator-Module\"

if /i [Development-Module] EQU [Ontolog-Module] (
	SET DOWNLOAD_URL=https://sourceforge.net/projects/ontologymanager/files/OntologyManager
) else (
	SET DOWNLOAD_URL=https://sourceforge.net/projects/ontologymanager/files/Modules/Development-Module
)

del /s /q "%OMODULE_PATH%\Development-Module\*.*"

wget -O "%temp%\OModules\Development-Module_0.5.3.262.exe" %DOWNLOAD_URL%/Development-Module_0.5.3.262.exe/download
"%PROGRAMFILES%\7-Zip\7z.exe" x "%temp%\OModules\Development-Module_0.5.3.262.exe" -o"%OMODULE_PATH%\Development-Module\"

if /i [DevelopmentVersionChange_Module] EQU [Ontolog-Module] (
	SET DOWNLOAD_URL=https://sourceforge.net/projects/ontologymanager/files/OntologyManager
) else (
	SET DOWNLOAD_URL=https://sourceforge.net/projects/ontologymanager/files/Modules/DevelopmentVersionChange_Module
)

del /s /q "%OMODULE_PATH%\DevelopmentVersionChange_Module\*.*"

wget -O "%temp%\OModules\DevelopmentVersionChange_Module_0.0.0.12.exe" %DOWNLOAD_URL%/DevelopmentVersionChange_Module_0.0.0.12.exe/download
"%PROGRAMFILES%\7-Zip\7z.exe" x "%temp%\OModules\DevelopmentVersionChange_Module_0.0.0.12.exe" -o"%OMODULE_PATH%\DevelopmentVersionChange_Module\"

if /i [DotNetLibrary-Connector] EQU [Ontolog-Module] (
	SET DOWNLOAD_URL=https://sourceforge.net/projects/ontologymanager/files/OntologyManager
) else (
	SET DOWNLOAD_URL=https://sourceforge.net/projects/ontologymanager/files/Modules/DotNetLibrary-Connector
)

del /s /q "%OMODULE_PATH%\DotNetLibrary-Connector\*.*"

wget -O "%temp%\OModules\DotNetLibrary-Connector_0.0.0.8.exe" %DOWNLOAD_URL%/DotNetLibrary-Connector_0.0.0.8.exe/download
"%PROGRAMFILES%\7-Zip\7z.exe" x "%temp%\OModules\DotNetLibrary-Connector_0.0.0.8.exe" -o"%OMODULE_PATH%\DotNetLibrary-Connector\"

if /i [ElasticSearchConfig-Module] EQU [Ontolog-Module] (
	SET DOWNLOAD_URL=https://sourceforge.net/projects/ontologymanager/files/OntologyManager
) else (
	SET DOWNLOAD_URL=https://sourceforge.net/projects/ontologymanager/files/Modules/ElasticSearchConfig-Module
)

del /s /q "%OMODULE_PATH%\ElasticSearchConfig-Module\*.*"

wget -O "%temp%\OModules\ElasticSearchConfig-Module_0.2.3.122.exe" %DOWNLOAD_URL%/ElasticSearchConfig-Module_0.2.3.122.exe/download
"%PROGRAMFILES%\7-Zip\7z.exe" x "%temp%\OModules\ElasticSearchConfig-Module_0.2.3.122.exe" -o"%OMODULE_PATH%\ElasticSearchConfig-Module\"

if /i [ElasticSearchLogging-Module] EQU [Ontolog-Module] (
	SET DOWNLOAD_URL=https://sourceforge.net/projects/ontologymanager/files/OntologyManager
) else (
	SET DOWNLOAD_URL=https://sourceforge.net/projects/ontologymanager/files/Modules/ElasticSearchLogging-Module
)

del /s /q "%OMODULE_PATH%\ElasticSearchLogging-Module\*.*"

wget -O "%temp%\OModules\ElasticSearchLogging-Module_0.2.3.127.exe" %DOWNLOAD_URL%/ElasticSearchLogging-Module_0.2.3.127.exe/download
"%PROGRAMFILES%\7-Zip\7z.exe" x "%temp%\OModules\ElasticSearchLogging-Module_0.2.3.127.exe" -o"%OMODULE_PATH%\ElasticSearchLogging-Module\"

if /i [FileResourceModule] EQU [Ontolog-Module] (
	SET DOWNLOAD_URL=https://sourceforge.net/projects/ontologymanager/files/OntologyManager
) else (
	SET DOWNLOAD_URL=https://sourceforge.net/projects/ontologymanager/files/Modules/FileResourceModule
)

del /s /q "%OMODULE_PATH%\FileResourceModule\*.*"

wget -O "%temp%\OModules\FileResourceModule_0.3.3.214.exe" %DOWNLOAD_URL%/FileResourceModule_0.3.3.214.exe/download
"%PROGRAMFILES%\7-Zip\7z.exe" x "%temp%\OModules\FileResourceModule_0.3.3.214.exe" -o"%OMODULE_PATH%\FileResourceModule\"

if /i [FileSystem-BaseModel] EQU [Ontolog-Module] (
	SET DOWNLOAD_URL=https://sourceforge.net/projects/ontologymanager/files/OntologyManager
) else (
	SET DOWNLOAD_URL=https://sourceforge.net/projects/ontologymanager/files/Modules/FileSystem-BaseModel
)

del /s /q "%OMODULE_PATH%\FileSystem-BaseModel\*.*"

wget -O "%temp%\OModules\FileSystem-BaseModel_0.2.3.88.exe" %DOWNLOAD_URL%/FileSystem-BaseModel_0.2.3.88.exe/download
"%PROGRAMFILES%\7-Zip\7z.exe" x "%temp%\OModules\FileSystem-BaseModel_0.2.3.88.exe" -o"%OMODULE_PATH%\FileSystem-BaseModel\"

if /i [FileSystem-Connector-Module] EQU [Ontolog-Module] (
	SET DOWNLOAD_URL=https://sourceforge.net/projects/ontologymanager/files/OntologyManager
) else (
	SET DOWNLOAD_URL=https://sourceforge.net/projects/ontologymanager/files/Modules/FileSystem-Connector-Module
)

del /s /q "%OMODULE_PATH%\FileSystem-Connector-Module\*.*"

wget -O "%temp%\OModules\FileSystem-Connector-Module_0.3.3.212.exe" %DOWNLOAD_URL%/FileSystem-Connector-Module_0.3.3.212.exe/download
"%PROGRAMFILES%\7-Zip\7z.exe" x "%temp%\OModules\FileSystem-Connector-Module_0.3.3.212.exe" -o"%OMODULE_PATH%\FileSystem-Connector-Module\"

if /i [Filesystem-Module] EQU [Ontolog-Module] (
	SET DOWNLOAD_URL=https://sourceforge.net/projects/ontologymanager/files/OntologyManager
) else (
	SET DOWNLOAD_URL=https://sourceforge.net/projects/ontologymanager/files/Modules/Filesystem-Module
)

del /s /q "%OMODULE_PATH%\Filesystem-Module\*.*"

wget -O "%temp%\OModules\Filesystem-Module_0.3.3.208.exe" %DOWNLOAD_URL%/Filesystem-Module_0.3.3.208.exe/download
"%PROGRAMFILES%\7-Zip\7z.exe" x "%temp%\OModules\Filesystem-Module_0.3.3.208.exe" -o"%OMODULE_PATH%\Filesystem-Module\"

if /i [File-Tagging-Module] EQU [Ontolog-Module] (
	SET DOWNLOAD_URL=https://sourceforge.net/projects/ontologymanager/files/OntologyManager
) else (
	SET DOWNLOAD_URL=https://sourceforge.net/projects/ontologymanager/files/Modules/File-Tagging-Module
)

del /s /q "%OMODULE_PATH%\File-Tagging-Module\*.*"

wget -O "%temp%\OModules\File-Tagging-Module_0.2.3.123.exe" %DOWNLOAD_URL%/File-Tagging-Module_0.2.3.123.exe/download
"%PROGRAMFILES%\7-Zip\7z.exe" x "%temp%\OModules\File-Tagging-Module_0.2.3.123.exe" -o"%OMODULE_PATH%\File-Tagging-Module\"

if /i [Formale-Begriffsanalyse-Module] EQU [Ontolog-Module] (
	SET DOWNLOAD_URL=https://sourceforge.net/projects/ontologymanager/files/OntologyManager
) else (
	SET DOWNLOAD_URL=https://sourceforge.net/projects/ontologymanager/files/Modules/Formale-Begriffsanalyse-Module
)

del /s /q "%OMODULE_PATH%\Formale-Begriffsanalyse-Module\*.*"

wget -O "%temp%\OModules\Formale-Begriffsanalyse-Module_0.2.3.121.exe" %DOWNLOAD_URL%/Formale-Begriffsanalyse-Module_0.2.3.121.exe/download
"%PROGRAMFILES%\7-Zip\7z.exe" x "%temp%\OModules\Formale-Begriffsanalyse-Module_0.2.3.121.exe" -o"%OMODULE_PATH%\Formale-Begriffsanalyse-Module\"

if /i [GraphMLConnector] EQU [Ontolog-Module] (
	SET DOWNLOAD_URL=https://sourceforge.net/projects/ontologymanager/files/OntologyManager
) else (
	SET DOWNLOAD_URL=https://sourceforge.net/projects/ontologymanager/files/Modules/GraphMLConnector
)

del /s /q "%OMODULE_PATH%\GraphMLConnector\*.*"

wget -O "%temp%\OModules\GraphMLConnector_0.2.3.173.exe" %DOWNLOAD_URL%/GraphMLConnector_0.2.3.173.exe/download
"%PROGRAMFILES%\7-Zip\7z.exe" x "%temp%\OModules\GraphMLConnector_0.2.3.173.exe" -o"%OMODULE_PATH%\GraphMLConnector\"

if /i [Grid-Module] EQU [Ontolog-Module] (
	SET DOWNLOAD_URL=https://sourceforge.net/projects/ontologymanager/files/OntologyManager
) else (
	SET DOWNLOAD_URL=https://sourceforge.net/projects/ontologymanager/files/Modules/Grid-Module
)

del /s /q "%OMODULE_PATH%\Grid-Module\*.*"

wget -O "%temp%\OModules\Grid-Module_0.2.3.79.exe" %DOWNLOAD_URL%/Grid-Module_0.2.3.79.exe/download
"%PROGRAMFILES%\7-Zip\7z.exe" x "%temp%\OModules\Grid-Module_0.2.3.79.exe" -o"%OMODULE_PATH%\Grid-Module\"

if /i [Hierarchichal-Splitter-Module] EQU [Ontolog-Module] (
	SET DOWNLOAD_URL=https://sourceforge.net/projects/ontologymanager/files/OntologyManager
) else (
	SET DOWNLOAD_URL=https://sourceforge.net/projects/ontologymanager/files/Modules/Hierarchichal-Splitter-Module
)

del /s /q "%OMODULE_PATH%\Hierarchichal-Splitter-Module\*.*"

wget -O "%temp%\OModules\Hierarchichal-Splitter-Module_0.2.3.125.exe" %DOWNLOAD_URL%/Hierarchichal-Splitter-Module_0.2.3.125.exe/download
"%PROGRAMFILES%\7-Zip\7z.exe" x "%temp%\OModules\Hierarchichal-Splitter-Module_0.2.3.125.exe" -o"%OMODULE_PATH%\Hierarchichal-Splitter-Module\"

if /i [HTMLEdit-Module] EQU [Ontolog-Module] (
	SET DOWNLOAD_URL=https://sourceforge.net/projects/ontologymanager/files/OntologyManager
) else (
	SET DOWNLOAD_URL=https://sourceforge.net/projects/ontologymanager/files/Modules/HTMLEdit-Module
)

del /s /q "%OMODULE_PATH%\HTMLEdit-Module\*.*"

wget -O "%temp%\OModules\HTMLEdit-Module_0.1.0.23.exe" %DOWNLOAD_URL%/HTMLEdit-Module_0.1.0.23.exe/download
"%PROGRAMFILES%\7-Zip\7z.exe" x "%temp%\OModules\HTMLEdit-Module_0.1.0.23.exe" -o"%OMODULE_PATH%\HTMLEdit-Module\"

if /i [HTMLExport-Module] EQU [Ontolog-Module] (
	SET DOWNLOAD_URL=https://sourceforge.net/projects/ontologymanager/files/OntologyManager
) else (
	SET DOWNLOAD_URL=https://sourceforge.net/projects/ontologymanager/files/Modules/HTMLExport-Module
)

del /s /q "%OMODULE_PATH%\HTMLExport-Module\*.*"

wget -O "%temp%\OModules\HTMLExport-Module_0.3.3.182.exe" %DOWNLOAD_URL%/HTMLExport-Module_0.3.3.182.exe/download
"%PROGRAMFILES%\7-Zip\7z.exe" x "%temp%\OModules\HTMLExport-Module_0.3.3.182.exe" -o"%OMODULE_PATH%\HTMLExport-Module\"

if /i [ImageMapper-Module] EQU [Ontolog-Module] (
	SET DOWNLOAD_URL=https://sourceforge.net/projects/ontologymanager/files/OntologyManager
) else (
	SET DOWNLOAD_URL=https://sourceforge.net/projects/ontologymanager/files/Modules/ImageMapper-Module
)

del /s /q "%OMODULE_PATH%\ImageMapper-Module\*.*"

wget -O "%temp%\OModules\ImageMapper-Module_0.1.0.16.exe" %DOWNLOAD_URL%/ImageMapper-Module_0.1.0.16.exe/download
"%PROGRAMFILES%\7-Zip\7z.exe" x "%temp%\OModules\ImageMapper-Module_0.1.0.16.exe" -o"%OMODULE_PATH%\ImageMapper-Module\"

if /i [ImageViewer-Module] EQU [Ontolog-Module] (
	SET DOWNLOAD_URL=https://sourceforge.net/projects/ontologymanager/files/OntologyManager
) else (
	SET DOWNLOAD_URL=https://sourceforge.net/projects/ontologymanager/files/Modules/ImageViewer-Module
)

del /s /q "%OMODULE_PATH%\ImageViewer-Module\*.*"

wget -O "%temp%\OModules\ImageViewer-Module_0.1.0.5.exe" %DOWNLOAD_URL%/ImageViewer-Module_0.1.0.5.exe/download
"%PROGRAMFILES%\7-Zip\7z.exe" x "%temp%\OModules\ImageViewer-Module_0.1.0.5.exe" -o"%OMODULE_PATH%\ImageViewer-Module\"

if /i [Import_RedMine-Projects] EQU [Ontolog-Module] (
	SET DOWNLOAD_URL=https://sourceforge.net/projects/ontologymanager/files/OntologyManager
) else (
	SET DOWNLOAD_URL=https://sourceforge.net/projects/ontologymanager/files/Modules/Import_RedMine-Projects
)

del /s /q "%OMODULE_PATH%\Import_RedMine-Projects\*.*"

wget -O "%temp%\OModules\Import_RedMine-Projects_0.3.1.123.exe" %DOWNLOAD_URL%/Import_RedMine-Projects_0.3.1.123.exe/download
"%PROGRAMFILES%\7-Zip\7z.exe" x "%temp%\OModules\Import_RedMine-Projects_0.3.1.123.exe" -o"%OMODULE_PATH%\Import_RedMine-Projects\"

if /i [LiteraturQuellen-Module] EQU [Ontolog-Module] (
	SET DOWNLOAD_URL=https://sourceforge.net/projects/ontologymanager/files/OntologyManager
) else (
	SET DOWNLOAD_URL=https://sourceforge.net/projects/ontologymanager/files/Modules/LiteraturQuellen-Module
)

del /s /q "%OMODULE_PATH%\LiteraturQuellen-Module\*.*"

wget -O "%temp%\OModules\LiteraturQuellen-Module_0.3.3.265.exe" %DOWNLOAD_URL%/LiteraturQuellen-Module_0.3.3.265.exe/download
"%PROGRAMFILES%\7-Zip\7z.exe" x "%temp%\OModules\LiteraturQuellen-Module_0.3.3.265.exe" -o"%OMODULE_PATH%\LiteraturQuellen-Module\"

if /i [Localization-Module] EQU [Ontolog-Module] (
	SET DOWNLOAD_URL=https://sourceforge.net/projects/ontologymanager/files/OntologyManager
) else (
	SET DOWNLOAD_URL=https://sourceforge.net/projects/ontologymanager/files/Modules/Localization-Module
)

del /s /q "%OMODULE_PATH%\Localization-Module\*.*"

wget -O "%temp%\OModules\Localization-Module_0.2.4.125.exe" %DOWNLOAD_URL%/Localization-Module_0.2.4.125.exe/download
"%PROGRAMFILES%\7-Zip\7z.exe" x "%temp%\OModules\Localization-Module_0.2.4.125.exe" -o"%OMODULE_PATH%\Localization-Module\"

if /i [LocalizedTemplate-Module] EQU [Ontolog-Module] (
	SET DOWNLOAD_URL=https://sourceforge.net/projects/ontologymanager/files/OntologyManager
) else (
	SET DOWNLOAD_URL=https://sourceforge.net/projects/ontologymanager/files/Modules/LocalizedTemplate-Module
)

del /s /q "%OMODULE_PATH%\LocalizedTemplate-Module\*.*"

wget -O "%temp%\OModules\LocalizedTemplate-Module_0.2.3.83.exe" %DOWNLOAD_URL%/LocalizedTemplate-Module_0.2.3.83.exe/download
"%PROGRAMFILES%\7-Zip\7z.exe" x "%temp%\OModules\LocalizedTemplate-Module_0.2.3.83.exe" -o"%OMODULE_PATH%\LocalizedTemplate-Module\"

if /i [Log-Module] EQU [Ontolog-Module] (
	SET DOWNLOAD_URL=https://sourceforge.net/projects/ontologymanager/files/OntologyManager
) else (
	SET DOWNLOAD_URL=https://sourceforge.net/projects/ontologymanager/files/Modules/Log-Module
)

del /s /q "%OMODULE_PATH%\Log-Module\*.*"

wget -O "%temp%\OModules\Log-Module_0.4.3.139.exe" %DOWNLOAD_URL%/Log-Module_0.4.3.139.exe/download
"%PROGRAMFILES%\7-Zip\7z.exe" x "%temp%\OModules\Log-Module_0.4.3.139.exe" -o"%OMODULE_PATH%\Log-Module\"

if /i [Manual-Repair-Module] EQU [Ontolog-Module] (
	SET DOWNLOAD_URL=https://sourceforge.net/projects/ontologymanager/files/OntologyManager
) else (
	SET DOWNLOAD_URL=https://sourceforge.net/projects/ontologymanager/files/Modules/Manual-Repair-Module
)

del /s /q "%OMODULE_PATH%\Manual-Repair-Module\*.*"

wget -O "%temp%\OModules\Manual-Repair-Module_0.2.3.118.exe" %DOWNLOAD_URL%/Manual-Repair-Module_0.2.3.118.exe/download
"%PROGRAMFILES%\7-Zip\7z.exe" x "%temp%\OModules\Manual-Repair-Module_0.2.3.118.exe" -o"%OMODULE_PATH%\Manual-Repair-Module\"

if /i [MediaStore-Module] EQU [Ontolog-Module] (
	SET DOWNLOAD_URL=https://sourceforge.net/projects/ontologymanager/files/OntologyManager
) else (
	SET DOWNLOAD_URL=https://sourceforge.net/projects/ontologymanager/files/Modules/MediaStore-Module
)

del /s /q "%OMODULE_PATH%\MediaStore-Module\*.*"

wget -O "%temp%\OModules\MediaStore-Module_0.1.0.14.exe" %DOWNLOAD_URL%/MediaStore-Module_0.1.0.14.exe/download
"%PROGRAMFILES%\7-Zip\7z.exe" x "%temp%\OModules\MediaStore-Module_0.1.0.14.exe" -o"%OMODULE_PATH%\MediaStore-Module\"

if /i [MediaViewerController] EQU [Ontolog-Module] (
	SET DOWNLOAD_URL=https://sourceforge.net/projects/ontologymanager/files/OntologyManager
) else (
	SET DOWNLOAD_URL=https://sourceforge.net/projects/ontologymanager/files/Modules/MediaViewerController
)

del /s /q "%OMODULE_PATH%\MediaViewerController\*.*"

wget -O "%temp%\OModules\MediaViewerController_0.1.0.1.exe" %DOWNLOAD_URL%/MediaViewerController_0.1.0.1.exe/download
"%PROGRAMFILES%\7-Zip\7z.exe" x "%temp%\OModules\MediaViewerController_0.1.0.1.exe" -o"%OMODULE_PATH%\MediaViewerController\"

if /i [Media-Viewer-Module] EQU [Ontolog-Module] (
	SET DOWNLOAD_URL=https://sourceforge.net/projects/ontologymanager/files/OntologyManager
) else (
	SET DOWNLOAD_URL=https://sourceforge.net/projects/ontologymanager/files/Modules/Media-Viewer-Module
)

del /s /q "%OMODULE_PATH%\Media-Viewer-Module\*.*"

wget -O "%temp%\OModules\Media-Viewer-Module_0.3.4.240.exe" %DOWNLOAD_URL%/Media-Viewer-Module_0.3.4.240.exe/download
"%PROGRAMFILES%\7-Zip\7z.exe" x "%temp%\OModules\Media-Viewer-Module_0.3.4.240.exe" -o"%OMODULE_PATH%\Media-Viewer-Module\"

if /i [Media-Web-Module] EQU [Ontolog-Module] (
	SET DOWNLOAD_URL=https://sourceforge.net/projects/ontologymanager/files/OntologyManager
) else (
	SET DOWNLOAD_URL=https://sourceforge.net/projects/ontologymanager/files/Modules/Media-Web-Module
)

del /s /q "%OMODULE_PATH%\Media-Web-Module\*.*"

wget -O "%temp%\OModules\Media-Web-Module_0.3.3.97.exe" %DOWNLOAD_URL%/Media-Web-Module_0.3.3.97.exe/download
"%PROGRAMFILES%\7-Zip\7z.exe" x "%temp%\OModules\Media-Web-Module_0.3.3.97.exe" -o"%OMODULE_PATH%\Media-Web-Module\"

if /i [ModuleLibrary] EQU [Ontolog-Module] (
	SET DOWNLOAD_URL=https://sourceforge.net/projects/ontologymanager/files/OntologyManager
) else (
	SET DOWNLOAD_URL=https://sourceforge.net/projects/ontologymanager/files/Modules/ModuleLibrary
)

del /s /q "%OMODULE_PATH%\ModuleLibrary\*.*"

wget -O "%temp%\OModules\ModuleLibrary_0.2.3.127.exe" %DOWNLOAD_URL%/ModuleLibrary_0.2.3.127.exe/download
"%PROGRAMFILES%\7-Zip\7z.exe" x "%temp%\OModules\ModuleLibrary_0.2.3.127.exe" -o"%OMODULE_PATH%\ModuleLibrary\"

if /i [NextGenerationOntoEdit] EQU [Ontolog-Module] (
	SET DOWNLOAD_URL=https://sourceforge.net/projects/ontologymanager/files/OntologyManager
) else (
	SET DOWNLOAD_URL=https://sourceforge.net/projects/ontologymanager/files/Modules/NextGenerationOntoEdit
)

del /s /q "%OMODULE_PATH%\NextGenerationOntoEdit\*.*"

wget -O "%temp%\OModules\NextGenerationOntoEdit_0.2.3.128.exe" %DOWNLOAD_URL%/NextGenerationOntoEdit_0.2.3.128.exe/download
"%PROGRAMFILES%\7-Zip\7z.exe" x "%temp%\OModules\NextGenerationOntoEdit_0.2.3.128.exe" -o"%OMODULE_PATH%\NextGenerationOntoEdit\"

if /i [Office-Module] EQU [Ontolog-Module] (
	SET DOWNLOAD_URL=https://sourceforge.net/projects/ontologymanager/files/OntologyManager
) else (
	SET DOWNLOAD_URL=https://sourceforge.net/projects/ontologymanager/files/Modules/Office-Module
)

del /s /q "%OMODULE_PATH%\Office-Module\*.*"

wget -O "%temp%\OModules\Office-Module_0.3.4.163.exe" %DOWNLOAD_URL%/Office-Module_0.3.4.163.exe/download
"%PROGRAMFILES%\7-Zip\7z.exe" x "%temp%\OModules\Office-Module_0.3.4.163.exe" -o"%OMODULE_PATH%\Office-Module\"

if /i [OItemListController] EQU [Ontolog-Module] (
	SET DOWNLOAD_URL=https://sourceforge.net/projects/ontologymanager/files/OntologyManager
) else (
	SET DOWNLOAD_URL=https://sourceforge.net/projects/ontologymanager/files/Modules/OItemListController
)

del /s /q "%OMODULE_PATH%\OItemListController\*.*"

wget -O "%temp%\OModules\OItemListController_0.1.0.1.exe" %DOWNLOAD_URL%/OItemListController_0.1.0.1.exe/download
"%PROGRAMFILES%\7-Zip\7z.exe" x "%temp%\OModules\OItemListController_0.1.0.1.exe" -o"%OMODULE_PATH%\OItemListController\"

if /i [OModulesControls] EQU [Ontolog-Module] (
	SET DOWNLOAD_URL=https://sourceforge.net/projects/ontologymanager/files/OntologyManager
) else (
	SET DOWNLOAD_URL=https://sourceforge.net/projects/ontologymanager/files/Modules/OModulesControls
)

del /s /q "%OMODULE_PATH%\OModulesControls\*.*"

wget -O "%temp%\OModules\OModulesControls_0.0.0.27.exe" %DOWNLOAD_URL%/OModulesControls_0.0.0.27.exe/download
"%PROGRAMFILES%\7-Zip\7z.exe" x "%temp%\OModules\OModulesControls_0.0.0.27.exe" -o"%OMODULE_PATH%\OModulesControls\"

if /i [OntologyClassCreator-Module] EQU [Ontolog-Module] (
	SET DOWNLOAD_URL=https://sourceforge.net/projects/ontologymanager/files/OntologyManager
) else (
	SET DOWNLOAD_URL=https://sourceforge.net/projects/ontologymanager/files/Modules/OntologyClassCreator-Module
)

del /s /q "%OMODULE_PATH%\OntologyClassCreator-Module\*.*"

wget -O "%temp%\OModules\OntologyClassCreator-Module_0.0.0.25.exe" %DOWNLOAD_URL%/OntologyClassCreator-Module_0.0.0.25.exe/download
"%PROGRAMFILES%\7-Zip\7z.exe" x "%temp%\OModules\OntologyClassCreator-Module_0.0.0.25.exe" -o"%OMODULE_PATH%\OntologyClassCreator-Module\"

if /i [OntologyClassEdit] EQU [Ontolog-Module] (
	SET DOWNLOAD_URL=https://sourceforge.net/projects/ontologymanager/files/OntologyManager
) else (
	SET DOWNLOAD_URL=https://sourceforge.net/projects/ontologymanager/files/Modules/OntologyClassEdit
)

del /s /q "%OMODULE_PATH%\OntologyClassEdit\*.*"

wget -O "%temp%\OModules\OntologyClassEdit_0.0.0.14.exe" %DOWNLOAD_URL%/OntologyClassEdit_0.0.0.14.exe/download
"%PROGRAMFILES%\7-Zip\7z.exe" x "%temp%\OModules\OntologyClassEdit_0.0.0.14.exe" -o"%OMODULE_PATH%\OntologyClassEdit\"

if /i [OntologyItemListModule] EQU [Ontolog-Module] (
	SET DOWNLOAD_URL=https://sourceforge.net/projects/ontologymanager/files/OntologyManager
) else (
	SET DOWNLOAD_URL=https://sourceforge.net/projects/ontologymanager/files/Modules/OntologyItemListModule
)

del /s /q "%OMODULE_PATH%\OntologyItemListModule\*.*"

wget -O "%temp%\OModules\OntologyItemListModule_0.0.0.14.exe" %DOWNLOAD_URL%/OntologyItemListModule_0.0.0.14.exe/download
"%PROGRAMFILES%\7-Zip\7z.exe" x "%temp%\OModules\OntologyItemListModule_0.0.0.14.exe" -o"%OMODULE_PATH%\OntologyItemListModule\"

if /i [Ontology-Module] EQU [Ontolog-Module] (
	SET DOWNLOAD_URL=https://sourceforge.net/projects/ontologymanager/files/OntologyManager
) else (
	SET DOWNLOAD_URL=https://sourceforge.net/projects/ontologymanager/files/Modules/Ontology-Module
)

del /s /q "%OMODULE_PATH%\Ontology-Module\*.*"

wget -O "%temp%\OModules\Ontology-Module_0.5.3.208.exe" %DOWNLOAD_URL%/Ontology-Module_0.5.3.208.exe/download
"%PROGRAMFILES%\7-Zip\7z.exe" x "%temp%\OModules\Ontology-Module_0.5.3.208.exe" -o"%OMODULE_PATH%\Ontology-Module\"

if /i [OntologySync-Module] EQU [Ontolog-Module] (
	SET DOWNLOAD_URL=https://sourceforge.net/projects/ontologymanager/files/OntologyManager
) else (
	SET DOWNLOAD_URL=https://sourceforge.net/projects/ontologymanager/files/Modules/OntologySync-Module
)

del /s /q "%OMODULE_PATH%\OntologySync-Module\*.*"

wget -O "%temp%\OModules\OntologySync-Module_0.1.3.77.exe" %DOWNLOAD_URL%/OntologySync-Module_0.1.3.77.exe/download
"%PROGRAMFILES%\7-Zip\7z.exe" x "%temp%\OModules\OntologySync-Module_0.1.3.77.exe" -o"%OMODULE_PATH%\OntologySync-Module\"

if /i [OntologyTreeModel-Module] EQU [Ontolog-Module] (
	SET DOWNLOAD_URL=https://sourceforge.net/projects/ontologymanager/files/OntologyManager
) else (
	SET DOWNLOAD_URL=https://sourceforge.net/projects/ontologymanager/files/Modules/OntologyTreeModel-Module
)

del /s /q "%OMODULE_PATH%\OntologyTreeModel-Module\*.*"

wget -O "%temp%\OModules\OntologyTreeModel-Module_0.1.0.8.exe" %DOWNLOAD_URL%/OntologyTreeModel-Module_0.1.0.8.exe/download
"%PROGRAMFILES%\7-Zip\7z.exe" x "%temp%\OModules\OntologyTreeModel-Module_0.1.0.8.exe" -o"%OMODULE_PATH%\OntologyTreeModel-Module\"

if /i [OntologyViewModels] EQU [Ontolog-Module] (
	SET DOWNLOAD_URL=https://sourceforge.net/projects/ontologymanager/files/OntologyManager
) else (
	SET DOWNLOAD_URL=https://sourceforge.net/projects/ontologymanager/files/Modules/OntologyViewModels
)

del /s /q "%OMODULE_PATH%\OntologyViewModels\*.*"

wget -O "%temp%\OModules\OntologyViewModels_0.0.0.14.exe" %DOWNLOAD_URL%/OntologyViewModels_0.0.0.14.exe/download
"%PROGRAMFILES%\7-Zip\7z.exe" x "%temp%\OModules\OntologyViewModels_0.0.0.14.exe" -o"%OMODULE_PATH%\OntologyViewModels\"

if /i [OntologyWebAppController] EQU [Ontolog-Module] (
	SET DOWNLOAD_URL=https://sourceforge.net/projects/ontologymanager/files/OntologyManager
) else (
	SET DOWNLOAD_URL=https://sourceforge.net/projects/ontologymanager/files/Modules/OntologyWebAppController
)

del /s /q "%OMODULE_PATH%\OntologyWebAppController\*.*"

wget -O "%temp%\OModules\OntologyWebAppController_0.1.0.2.exe" %DOWNLOAD_URL%/OntologyWebAppController_0.1.0.2.exe/download
"%PROGRAMFILES%\7-Zip\7z.exe" x "%temp%\OModules\OntologyWebAppController_0.1.0.2.exe" -o"%OMODULE_PATH%\OntologyWebAppController\"

if /i [OntoWebSocketServer] EQU [Ontolog-Module] (
	SET DOWNLOAD_URL=https://sourceforge.net/projects/ontologymanager/files/OntologyManager
) else (
	SET DOWNLOAD_URL=https://sourceforge.net/projects/ontologymanager/files/Modules/OntoWebSocketServer
)

del /s /q "%OMODULE_PATH%\OntoWebSocketServer\*.*"

wget -O "%temp%\OModules\OntoWebSocketServer_0.0.0.1.exe" %DOWNLOAD_URL%/OntoWebSocketServer_0.0.0.1.exe/download
"%PROGRAMFILES%\7-Zip\7z.exe" x "%temp%\OModules\OntoWebSocketServer_0.0.0.1.exe" -o"%OMODULE_PATH%\OntoWebSocketServer\"

if /i [OutlookConnector-Module] EQU [Ontolog-Module] (
	SET DOWNLOAD_URL=https://sourceforge.net/projects/ontologymanager/files/OntologyManager
) else (
	SET DOWNLOAD_URL=https://sourceforge.net/projects/ontologymanager/files/Modules/OutlookConnector-Module
)

del /s /q "%OMODULE_PATH%\OutlookConnector-Module\*.*"

wget -O "%temp%\OModules\OutlookConnector-Module_0.3.3.215.exe" %DOWNLOAD_URL%/OutlookConnector-Module_0.3.3.215.exe/download
"%PROGRAMFILES%\7-Zip\7z.exe" x "%temp%\OModules\OutlookConnector-Module_0.3.3.215.exe" -o"%OMODULE_PATH%\OutlookConnector-Module\"

if /i [Partner-Module] EQU [Ontolog-Module] (
	SET DOWNLOAD_URL=https://sourceforge.net/projects/ontologymanager/files/OntologyManager
) else (
	SET DOWNLOAD_URL=https://sourceforge.net/projects/ontologymanager/files/Modules/Partner-Module
)

del /s /q "%OMODULE_PATH%\Partner-Module\*.*"

wget -O "%temp%\OModules\Partner-Module_0.5.3.204.exe" %DOWNLOAD_URL%/Partner-Module_0.5.3.204.exe/download
"%PROGRAMFILES%\7-Zip\7z.exe" x "%temp%\OModules\Partner-Module_0.5.3.204.exe" -o"%OMODULE_PATH%\Partner-Module\"

if /i [Ping-Test-Module] EQU [Ontolog-Module] (
	SET DOWNLOAD_URL=https://sourceforge.net/projects/ontologymanager/files/OntologyManager
) else (
	SET DOWNLOAD_URL=https://sourceforge.net/projects/ontologymanager/files/Modules/Ping-Test-Module
)

del /s /q "%OMODULE_PATH%\Ping-Test-Module\*.*"

wget -O "%temp%\OModules\Ping-Test-Module_0.2.3.114.exe" %DOWNLOAD_URL%/Ping-Test-Module_0.2.3.114.exe/download
"%PROGRAMFILES%\7-Zip\7z.exe" x "%temp%\OModules\Ping-Test-Module_0.2.3.114.exe" -o"%OMODULE_PATH%\Ping-Test-Module\"

if /i [PortListenerForText-Module] EQU [Ontolog-Module] (
	SET DOWNLOAD_URL=https://sourceforge.net/projects/ontologymanager/files/OntologyManager
) else (
	SET DOWNLOAD_URL=https://sourceforge.net/projects/ontologymanager/files/Modules/PortListenerForText-Module
)

del /s /q "%OMODULE_PATH%\PortListenerForText-Module\*.*"

wget -O "%temp%\OModules\PortListenerForText-Module_0.2.3.91.exe" %DOWNLOAD_URL%/PortListenerForText-Module_0.2.3.91.exe/download
"%PROGRAMFILES%\7-Zip\7z.exe" x "%temp%\OModules\PortListenerForText-Module_0.2.3.91.exe" -o"%OMODULE_PATH%\PortListenerForText-Module\"

if /i [Priority-Tagging-Module] EQU [Ontolog-Module] (
	SET DOWNLOAD_URL=https://sourceforge.net/projects/ontologymanager/files/OntologyManager
) else (
	SET DOWNLOAD_URL=https://sourceforge.net/projects/ontologymanager/files/Modules/Priority-Tagging-Module
)

del /s /q "%OMODULE_PATH%\Priority-Tagging-Module\*.*"

wget -O "%temp%\OModules\Priority-Tagging-Module_0.0.0.9.exe" %DOWNLOAD_URL%/Priority-Tagging-Module_0.0.0.9.exe/download
"%PROGRAMFILES%\7-Zip\7z.exe" x "%temp%\OModules\Priority-Tagging-Module_0.0.0.9.exe" -o"%OMODULE_PATH%\Priority-Tagging-Module\"

if /i [Process-Module] EQU [Ontolog-Module] (
	SET DOWNLOAD_URL=https://sourceforge.net/projects/ontologymanager/files/OntologyManager
) else (
	SET DOWNLOAD_URL=https://sourceforge.net/projects/ontologymanager/files/Modules/Process-Module
)

del /s /q "%OMODULE_PATH%\Process-Module\*.*"

wget -O "%temp%\OModules\Process-Module_0.5.3.207.exe" %DOWNLOAD_URL%/Process-Module_0.5.3.207.exe/download
"%PROGRAMFILES%\7-Zip\7z.exe" x "%temp%\OModules\Process-Module_0.5.3.207.exe" -o"%OMODULE_PATH%\Process-Module\"

if /i [ProcessTree-Module] EQU [Ontolog-Module] (
	SET DOWNLOAD_URL=https://sourceforge.net/projects/ontologymanager/files/OntologyManager
) else (
	SET DOWNLOAD_URL=https://sourceforge.net/projects/ontologymanager/files/Modules/ProcessTree-Module
)

del /s /q "%OMODULE_PATH%\ProcessTree-Module\*.*"

wget -O "%temp%\OModules\ProcessTree-Module_0.0.0.1.exe" %DOWNLOAD_URL%/ProcessTree-Module_0.0.0.1.exe/download
"%PROGRAMFILES%\7-Zip\7z.exe" x "%temp%\OModules\ProcessTree-Module_0.0.0.1.exe" -o"%OMODULE_PATH%\ProcessTree-Module\"

if /i [RDF-Module] EQU [Ontolog-Module] (
	SET DOWNLOAD_URL=https://sourceforge.net/projects/ontologymanager/files/OntologyManager
) else (
	SET DOWNLOAD_URL=https://sourceforge.net/projects/ontologymanager/files/Modules/RDF-Module
)

del /s /q "%OMODULE_PATH%\RDF-Module\*.*"

wget -O "%temp%\OModules\RDF-Module_0.2.3.152.exe" %DOWNLOAD_URL%/RDF-Module_0.2.3.152.exe/download
"%PROGRAMFILES%\7-Zip\7z.exe" x "%temp%\OModules\RDF-Module_0.2.3.152.exe" -o"%OMODULE_PATH%\RDF-Module\"

if /i [Report-Module] EQU [Ontolog-Module] (
	SET DOWNLOAD_URL=https://sourceforge.net/projects/ontologymanager/files/OntologyManager
) else (
	SET DOWNLOAD_URL=https://sourceforge.net/projects/ontologymanager/files/Modules/Report-Module
)

del /s /q "%OMODULE_PATH%\Report-Module\*.*"

wget -O "%temp%\OModules\Report-Module_0.3.4.265.exe" %DOWNLOAD_URL%/Report-Module_0.3.4.265.exe/download
"%PROGRAMFILES%\7-Zip\7z.exe" x "%temp%\OModules\Report-Module_0.3.4.265.exe" -o"%OMODULE_PATH%\Report-Module\"

if /i [ReportViewToWordFormLetter-Module] EQU [Ontolog-Module] (
	SET DOWNLOAD_URL=https://sourceforge.net/projects/ontologymanager/files/OntologyManager
) else (
	SET DOWNLOAD_URL=https://sourceforge.net/projects/ontologymanager/files/Modules/ReportViewToWordFormLetter-Module
)

del /s /q "%OMODULE_PATH%\ReportViewToWordFormLetter-Module\*.*"

wget -O "%temp%\OModules\ReportViewToWordFormLetter-Module_0.0.0.1.exe" %DOWNLOAD_URL%/ReportViewToWordFormLetter-Module_0.0.0.1.exe/download
"%PROGRAMFILES%\7-Zip\7z.exe" x "%temp%\OModules\ReportViewToWordFormLetter-Module_0.0.0.1.exe" -o"%OMODULE_PATH%\ReportViewToWordFormLetter-Module\"

if /i [Schriftverkehrs-Module] EQU [Ontolog-Module] (
	SET DOWNLOAD_URL=https://sourceforge.net/projects/ontologymanager/files/OntologyManager
) else (
	SET DOWNLOAD_URL=https://sourceforge.net/projects/ontologymanager/files/Modules/Schriftverkehrs-Module
)

del /s /q "%OMODULE_PATH%\Schriftverkehrs-Module\*.*"

wget -O "%temp%\OModules\Schriftverkehrs-Module_0.3.3.261.exe" %DOWNLOAD_URL%/Schriftverkehrs-Module_0.3.3.261.exe/download
"%PROGRAMFILES%\7-Zip\7z.exe" x "%temp%\OModules\Schriftverkehrs-Module_0.3.3.261.exe" -o"%OMODULE_PATH%\Schriftverkehrs-Module\"

if /i [ScriptingModule] EQU [Ontolog-Module] (
	SET DOWNLOAD_URL=https://sourceforge.net/projects/ontologymanager/files/OntologyManager
) else (
	SET DOWNLOAD_URL=https://sourceforge.net/projects/ontologymanager/files/Modules/ScriptingModule
)

del /s /q "%OMODULE_PATH%\ScriptingModule\*.*"

wget -O "%temp%\OModules\ScriptingModule_0.3.3.126.exe" %DOWNLOAD_URL%/ScriptingModule_0.3.3.126.exe/download
"%PROGRAMFILES%\7-Zip\7z.exe" x "%temp%\OModules\ScriptingModule_0.3.3.126.exe" -o"%OMODULE_PATH%\ScriptingModule\"

if /i [SD-Classes-Visualizer] EQU [Ontolog-Module] (
	SET DOWNLOAD_URL=https://sourceforge.net/projects/ontologymanager/files/OntologyManager
) else (
	SET DOWNLOAD_URL=https://sourceforge.net/projects/ontologymanager/files/Modules/SD-Classes-Visualizer
)

del /s /q "%OMODULE_PATH%\SD-Classes-Visualizer\*.*"

wget -O "%temp%\OModules\SD-Classes-Visualizer_0.0.0.43.exe" %DOWNLOAD_URL%/SD-Classes-Visualizer_0.0.0.43.exe/download
"%PROGRAMFILES%\7-Zip\7z.exe" x "%temp%\OModules\SD-Classes-Visualizer_0.0.0.43.exe" -o"%OMODULE_PATH%\SD-Classes-Visualizer\"

if /i [Security-Module] EQU [Ontolog-Module] (
	SET DOWNLOAD_URL=https://sourceforge.net/projects/ontologymanager/files/OntologyManager
) else (
	SET DOWNLOAD_URL=https://sourceforge.net/projects/ontologymanager/files/Modules/Security-Module
)

del /s /q "%OMODULE_PATH%\Security-Module\*.*"

wget -O "%temp%\OModules\Security-Module_0.4.3.124.exe" %DOWNLOAD_URL%/Security-Module_0.4.3.124.exe/download
"%PROGRAMFILES%\7-Zip\7z.exe" x "%temp%\OModules\Security-Module_0.4.3.124.exe" -o"%OMODULE_PATH%\Security-Module\"

if /i [SequenceDiagramVisualizer-Module] EQU [Ontolog-Module] (
	SET DOWNLOAD_URL=https://sourceforge.net/projects/ontologymanager/files/OntologyManager
) else (
	SET DOWNLOAD_URL=https://sourceforge.net/projects/ontologymanager/files/Modules/SequenceDiagramVisualizer-Module
)

del /s /q "%OMODULE_PATH%\SequenceDiagramVisualizer-Module\*.*"

wget -O "%temp%\OModules\SequenceDiagramVisualizer-Module_0.0.0.1.exe" %DOWNLOAD_URL%/SequenceDiagramVisualizer-Module_0.0.0.1.exe/download
"%PROGRAMFILES%\7-Zip\7z.exe" x "%temp%\OModules\SequenceDiagramVisualizer-Module_0.0.0.1.exe" -o"%OMODULE_PATH%\SequenceDiagramVisualizer-Module\"

if /i [TestAutomation-Module] EQU [Ontolog-Module] (
	SET DOWNLOAD_URL=https://sourceforge.net/projects/ontologymanager/files/OntologyManager
) else (
	SET DOWNLOAD_URL=https://sourceforge.net/projects/ontologymanager/files/Modules/TestAutomation-Module
)

del /s /q "%OMODULE_PATH%\TestAutomation-Module\*.*"

wget -O "%temp%\OModules\TestAutomation-Module_0.1.1.12.exe" %DOWNLOAD_URL%/TestAutomation-Module_0.1.1.12.exe/download
"%PROGRAMFILES%\7-Zip\7z.exe" x "%temp%\OModules\TestAutomation-Module_0.1.1.12.exe" -o"%OMODULE_PATH%\TestAutomation-Module\"

if /i [TextParser] EQU [Ontolog-Module] (
	SET DOWNLOAD_URL=https://sourceforge.net/projects/ontologymanager/files/OntologyManager
) else (
	SET DOWNLOAD_URL=https://sourceforge.net/projects/ontologymanager/files/Modules/TextParser
)

del /s /q "%OMODULE_PATH%\TextParser\*.*"

wget -O "%temp%\OModules\TextParser_0.3.3.231.exe" %DOWNLOAD_URL%/TextParser_0.3.3.231.exe/download
"%PROGRAMFILES%\7-Zip\7z.exe" x "%temp%\OModules\TextParser_0.3.3.231.exe" -o"%OMODULE_PATH%\TextParser\"

if /i [TextParserService-Module] EQU [Ontolog-Module] (
	SET DOWNLOAD_URL=https://sourceforge.net/projects/ontologymanager/files/OntologyManager
) else (
	SET DOWNLOAD_URL=https://sourceforge.net/projects/ontologymanager/files/Modules/TextParserService-Module
)

del /s /q "%OMODULE_PATH%\TextParserService-Module\*.*"

wget -O "%temp%\OModules\TextParserService-Module_0.1.1.82.exe" %DOWNLOAD_URL%/TextParserService-Module_0.1.1.82.exe/download
"%PROGRAMFILES%\7-Zip\7z.exe" x "%temp%\OModules\TextParserService-Module_0.1.1.82.exe" -o"%OMODULE_PATH%\TextParserService-Module\"

if /i [TimeManagement-Module] EQU [Ontolog-Module] (
	SET DOWNLOAD_URL=https://sourceforge.net/projects/ontologymanager/files/OntologyManager
) else (
	SET DOWNLOAD_URL=https://sourceforge.net/projects/ontologymanager/files/Modules/TimeManagement-Module
)

del /s /q "%OMODULE_PATH%\TimeManagement-Module\*.*"

wget -O "%temp%\OModules\TimeManagement-Module_0.2.3.169.exe" %DOWNLOAD_URL%/TimeManagement-Module_0.2.3.169.exe/download
"%PROGRAMFILES%\7-Zip\7z.exe" x "%temp%\OModules\TimeManagement-Module_0.2.3.169.exe" -o"%OMODULE_PATH%\TimeManagement-Module\"

if /i [Typed-Tagging-Module] EQU [Ontolog-Module] (
	SET DOWNLOAD_URL=https://sourceforge.net/projects/ontologymanager/files/OntologyManager
) else (
	SET DOWNLOAD_URL=https://sourceforge.net/projects/ontologymanager/files/Modules/Typed-Tagging-Module
)

del /s /q "%OMODULE_PATH%\Typed-Tagging-Module\*.*"

wget -O "%temp%\OModules\Typed-Tagging-Module_0.3.3.187.exe" %DOWNLOAD_URL%/Typed-Tagging-Module_0.3.3.187.exe/download
"%PROGRAMFILES%\7-Zip\7z.exe" x "%temp%\OModules\Typed-Tagging-Module_0.3.3.187.exe" -o"%OMODULE_PATH%\Typed-Tagging-Module\"

if /i [UseCase-Visualizer] EQU [Ontolog-Module] (
	SET DOWNLOAD_URL=https://sourceforge.net/projects/ontologymanager/files/OntologyManager
) else (
	SET DOWNLOAD_URL=https://sourceforge.net/projects/ontologymanager/files/Modules/UseCase-Visualizer
)

del /s /q "%OMODULE_PATH%\UseCase-Visualizer\*.*"

wget -O "%temp%\OModules\UseCase-Visualizer_0.0.0.36.exe" %DOWNLOAD_URL%/UseCase-Visualizer_0.0.0.36.exe/download
"%PROGRAMFILES%\7-Zip\7z.exe" x "%temp%\OModules\UseCase-Visualizer_0.0.0.36.exe" -o"%OMODULE_PATH%\UseCase-Visualizer\"

if /i [Variable-Value-Module] EQU [Ontolog-Module] (
	SET DOWNLOAD_URL=https://sourceforge.net/projects/ontologymanager/files/OntologyManager
) else (
	SET DOWNLOAD_URL=https://sourceforge.net/projects/ontologymanager/files/Modules/Variable-Value-Module
)

del /s /q "%OMODULE_PATH%\Variable-Value-Module\*.*"

wget -O "%temp%\OModules\Variable-Value-Module_0.2.3.112.exe" %DOWNLOAD_URL%/Variable-Value-Module_0.2.3.112.exe/download
"%PROGRAMFILES%\7-Zip\7z.exe" x "%temp%\OModules\Variable-Value-Module_0.2.3.112.exe" -o"%OMODULE_PATH%\Variable-Value-Module\"

if /i [Version-Module] EQU [Ontolog-Module] (
	SET DOWNLOAD_URL=https://sourceforge.net/projects/ontologymanager/files/OntologyManager
) else (
	SET DOWNLOAD_URL=https://sourceforge.net/projects/ontologymanager/files/Modules/Version-Module
)

del /s /q "%OMODULE_PATH%\Version-Module\*.*"

wget -O "%temp%\OModules\Version-Module_0.2.4.136.exe" %DOWNLOAD_URL%/Version-Module_0.2.4.136.exe/download
"%PROGRAMFILES%\7-Zip\7z.exe" x "%temp%\OModules\Version-Module_0.2.4.136.exe" -o"%OMODULE_PATH%\Version-Module\"

if /i [WebPageParser-Module] EQU [Ontolog-Module] (
	SET DOWNLOAD_URL=https://sourceforge.net/projects/ontologymanager/files/OntologyManager
) else (
	SET DOWNLOAD_URL=https://sourceforge.net/projects/ontologymanager/files/Modules/WebPageParser-Module
)

del /s /q "%OMODULE_PATH%\WebPageParser-Module\*.*"

wget -O "%temp%\OModules\WebPageParser-Module_0.1.0.12.exe" %DOWNLOAD_URL%/WebPageParser-Module_0.1.0.12.exe/download
"%PROGRAMFILES%\7-Zip\7z.exe" x "%temp%\OModules\WebPageParser-Module_0.1.0.12.exe" -o"%OMODULE_PATH%\WebPageParser-Module\"

if /i [WordPress-Module] EQU [Ontolog-Module] (
	SET DOWNLOAD_URL=https://sourceforge.net/projects/ontologymanager/files/OntologyManager
) else (
	SET DOWNLOAD_URL=https://sourceforge.net/projects/ontologymanager/files/Modules/WordPress-Module
)

del /s /q "%OMODULE_PATH%\WordPress-Module\*.*"

wget -O "%temp%\OModules\WordPress-Module_0.0.0.7.exe" %DOWNLOAD_URL%/WordPress-Module_0.0.0.7.exe/download
"%PROGRAMFILES%\7-Zip\7z.exe" x "%temp%\OModules\WordPress-Module_0.0.0.7.exe" -o"%OMODULE_PATH%\WordPress-Module\"

if /i [WordpressSync-Module] EQU [Ontolog-Module] (
	SET DOWNLOAD_URL=https://sourceforge.net/projects/ontologymanager/files/OntologyManager
) else (
	SET DOWNLOAD_URL=https://sourceforge.net/projects/ontologymanager/files/Modules/WordpressSync-Module
)

del /s /q "%OMODULE_PATH%\WordpressSync-Module\*.*"

wget -O "%temp%\OModules\WordpressSync-Module_0.0.0.7.exe" %DOWNLOAD_URL%/WordpressSync-Module_0.0.0.7.exe/download
"%PROGRAMFILES%\7-Zip\7z.exe" x "%temp%\OModules\WordpressSync-Module_0.0.0.7.exe" -o"%OMODULE_PATH%\WordpressSync-Module\"
